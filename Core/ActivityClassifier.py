import pandas as pd
import numpy as np
import torch
import pickle
from torch.optim import lr_scheduler
from torch.utils.data import Dataset, DataLoader
from .utilities.fast_tools import fast_personwise_normalize_body_25

class ActivityClassifier():
    def __init__(self, num_of_classes, input_size=50, seq_length = 3, hidden_layer_size=512, weights_path = "Core/ClassifierModel/MEGAGURA91.pt"):
        self.net = ActivityClassifier.Net(input_size=input_size, 
                                           seq_length=seq_length, 
                                           hidden_layer_size=hidden_layer_size, 
                                           output_size=num_of_classes)
        
        self.human_shape = (seq_length, input_size)
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        print(self.device)
        if torch.cuda.is_available():
            self.net.load_state_dict(torch.load(weights_path))
            self.net.cuda()
        else:
            self.net.load_state_dict(torch.load(weights_path,
                                                map_location=torch.device('cpu')))    
    
    class Net(torch.nn.Module):
        def __init__(self, 
                     input_size=50,
                     seq_length = 3,
                     hidden_layer_size=512, 
                     output_size=5,
                     batch_first=True):
            super().__init__()
            self.seq_length = seq_length
            self.hidden_layer_size = hidden_layer_size

            self.lstm = torch.nn.LSTM(input_size,
                                      hidden_layer_size,
                                      num_layers=1,
                                      batch_first=batch_first)
            torch.nn.init.xavier_normal_(self.lstm.weight_ih_l0, gain=1.0)
            torch.nn.init.xavier_normal_(self.lstm.weight_hh_l0, gain=1.0)
            self.rel = torch.nn.ReLU()
            self.bthnorm = torch.nn.BatchNorm1d(num_features=seq_length,
                                                eps=0.001,
                                                momentum=0.99)

            self.linear = torch.nn.Linear(hidden_layer_size*seq_length,
                                          output_size)
            self.drop = torch.nn.Dropout(p=0.7)

            self.softmax = torch.nn.Softmax(1)

            self.hidden_cell = (torch.zeros(1,
                                            seq_length,
                                            self.hidden_layer_size),
                                torch.zeros(1,
                                            seq_length,
                                            self.hidden_layer_size))

        def rebuildСell(self, input_seq, device):
            self.hidden_cell = [torch.zeros(1,
                                            len(input_seq),
                                            self.hidden_layer_size),
                                torch.zeros(1,
                                            len(input_seq),
                                            self.hidden_layer_size)] 
            self.hidden_cell[0] = self.hidden_cell[0].to(device)
            self.hidden_cell[1] = self.hidden_cell[1].to(device)

        def forward(self, input_seq):
            lstm_out, self.hidden_cell = self.lstm(input_seq.view(len(input_seq),
                                                                  len(input_seq[0]),
                                                                  -1),
                                                    self.hidden_cell)

            lstm_out = self.rel(lstm_out)
            lstm_out = self.bthnorm(lstm_out)
            lstm_out = self.drop(lstm_out)
            predictions = self.linear(lstm_out.reshape(len(input_seq),
                                                       self.hidden_layer_size*self.seq_length))
            predictions = self.softmax(predictions)
            return predictions
    
    class KeypointsDataset(Dataset):
        def __init__(self, X, Y, transform=None):
            self.X = X
            self.Y = Y
            
        def __len__(self):
            return len(self.X)
        
        def __getitem__(self, idx):
            x = self.X[idx]
            y = self.Y[idx]
            return x, y
    
    def setNet(self, net, num_of_classes, input_size=50, seq_length=3):
        self.human_shape = (seq_length, input_size)
        self.net = net
        self.net.to(self.device)
    
    def setWeights(self, weights_path):
        if torch.cuda.is_available():
            self.net.load_state_dict(torch.load(weights_path))
            self.net.cuda()
        else:
            self.net.load_state_dict(torch.load(weights_path,
                                                map_location=torch.device('cpu')))

    def predictAction(self, points):
        data = torch.FloatTensor(points)
        data = data.view(len(points),self.human_shape[0], self.human_shape[1])
        self.net.rebuildСell(data, self.device)
        data = data.to(self.device)
        return self.net(data).data.cpu().numpy()
        
    def saveNet(self, out_path):
        torch.save(self.net.state_dict(), out_path)
    
    def trainNet(self,
                 train_path,
                 val_path,
                 loss_function=None,
                 epochs=5,
                 optimizer_class=None,
                 optimizer_args=None,
                 scheduler=None,
                 sheduler_args=None,
                 limit=90,
                 batch_size=850):
        
        def trainEpoch(self, loader, loss_func, optimizer):
            m = self.net
            m.train()
            correct = 0
            for i, (data, mark) in enumerate(loader):
                data = data.to(self.device)
                mark = mark.to(self.device)
                m.rebuildСell(data, self.device)
                output = m(data)
                pred = output.data.max(1, keepdim=True)[1]
                correct += pred.eq(mark.data.view_as(pred)).sum()
                loss = loss_func(output, mark.type(torch.LongTensor).to(self.device))
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
            acc = 100.0 * correct / len(loader.dataset)
            return acc, loss

        def validEpoch(self, loader, loss_func):
            m = self.net
            m.eval()
            correct = 0
            for i, (data, mark) in enumerate(loader):
                data = data.to(self.device)
                mark = mark.to(self.device)
                m.rebuildСell(data, self.device)
                output = m(data)
                pred = output.data.max(1, keepdim=True)[1]
                correct += pred.eq(mark.data.view_as(pred)).sum()
                loss = loss_func(output, mark.type(torch.LongTensor).to(self.device))
            acc = 100.0 * correct / len(loader.dataset)
            return acc, loss     
        
        train_window = self.human_shape[0]
        X, Y = self.getDataFromCSV(train_path)
        vX, vY = self.getDataFromCSV(val_path)
        
        X = X.astype(np.float32)
        vX = vX.astype(np.float32)
        Y = Y.astype(int)
        vY = vY.astype(int)
        
        dataset = self.KeypointsDataset(X,Y)
        val_dataset = self.KeypointsDataset(vX,vY)
        
        if not loss_function:
            loss_function = torch.nn.CrossEntropyLoss()
        loss_function = loss_function.to(self.device)

        self.net = self.net.to(self.device)

        if optimizer_class:
            optimizer = optimizer_class(self.net.parameters(), **optimizer_args)
        else:
            optimizer = torch.optim.Adam(self.net.parameters(), lr=0.001)
        if scheduler:
            scheduler = scheduler(optimizer, **sheduler_args)
        val_accs = []
        train_accs = []
        val_losses = []
        train_losses = []

        for epoch in range(epochs):
            train_loader = torch.utils.data.DataLoader(dataset=dataset, batch_size=batch_size, shuffle=False)
            val_loader = torch.utils.data.DataLoader(dataset=val_dataset, batch_size=batch_size, shuffle=False) 
            # процесс тренировки
            train_acc, train_loss = trainEpoch(self, train_loader, loss_function, optimizer)
            val_acc, val_loss = validEpoch(self, val_loader, loss_function)

            val_accs.append(val_acc)
            train_accs.append(train_acc)
            val_losses.append(val_loss)
            train_losses.append(train_loss)

            print(f'Эпоха тренировки: {epoch} Train Acc: {train_acc:.6f} Train Loss: {train_loss.data.item():.6f} Val Acc: {val_acc:.6f} Val Loss: {val_loss.data.item():.6f}')
            
            if val_acc>limit:
                break
            if scheduler:
                scheduler.step(train_acc)
        return (train_losses, val_losses, train_accs, val_accs)
        
    def getDataFromCSV(self, in_path):
        raw_data = pd.read_csv(in_path)    
        raw_X = raw_data.to_numpy()
        X = np.delete(raw_X, -1, 1).tolist() #1 - col_mode, -1 - col_number
        num_of_classes = raw_data['Class'].nunique()
        raw_Y = raw_data['Class'].to_numpy()
        Y = []
        X = np.reshape(X,(-1, int(self.human_shape[1]/2), 2))
        for i in range(len(X)):
            X[i] = fast_personwise_normalize_body_25(X[i].copy()).reshape(int(self.human_shape[1]/2), 2) #DANGER
        X = X.reshape(-1,50)
        
        for val in raw_Y:
            Y.append(val)
        X = np.expand_dims(X, 1)
        X = X[:len(X)//self.human_shape[0]*self.human_shape[0]].reshape(-1, self.human_shape[0], self.human_shape[1])
        Y = np.array(Y)
        Y = Y[:len(Y)//self.human_shape[0]*self.human_shape[0]][::self.human_shape[0]]
        return X, Y
       
