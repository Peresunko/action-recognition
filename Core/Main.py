from .VideoReader import *
from .VideoWriter import *
from .Visualizer import *
from .ActivityClassifier import *
from .Tracker import *
from .PoseEstimator import *
import pandas as pd
import numpy as np
from .gpu_openpose import pyopenpose as op
from time import time
import cv2
import os
from .utilities.fast_tools import fast_personwise_normalize_all, fast_personwise_normalize_all_body_25

class Main():
    def __init__(self, names_of_classes, max_persons_count, 
                 pose_estimator_params=None, human_shape=(25, 2), 
                 frame_steps=3, track_limb=(1, 8),
                 classifier_neurons=512, classifier_weights_path=None):
        
        self.vr = VideoReader()
        self.vw = VideoWriter()
        self.vs = Visualizer()       
        
        if not classifier_weights_path:
            classifier_weights_path = "Core/ClassifierModel/MEGAGURA91.pt"
        
        self.pe = PoseEstimator(pose_estimator_params)
        
        self.tr = Tracker(max_persons_count=max_persons_count, 
                          frame_steps=frame_steps,
                          human_shape=human_shape, 
                          track_limb=track_limb)
        
        self.ac = ActivityClassifier(len(names_of_classes),
                                     input_size=human_shape[0]*human_shape[1],
                                     seq_length=frame_steps,
                                     hidden_layer_size=classifier_neurons,
                                     weights_path=classifier_weights_path)
        
        self.names_of_classes = names_of_classes
        self.rect_colors = (len(names_of_classes)-1)*[(255, 255, 255)] + [(0, 0, 255)]
    
    def processVideoAndShow(self, in_paths=[0], frames_in_row=2, skip_frames=3, queue_size=32, frame_transform=None):
        self.vr.setSources(srcs=in_paths, 
                           queue_size=queue_size, 
                           transform=frame_transform)
        
        fps = self.vr.getFPS()        
        if not fps:
            fps = 20
        delay = 1000/fps #ms

        self.tr.resetState()
        dummy_slots = self.tr.getSlotsCopy() #сюда будут помещаться нормализованные скелеты людей
        
        return_flag, frames = self.vr.getFrames()  
   
        self.vs.setFrameGrid(frames_in_row, len(in_paths), *frames[0].shape)
        while return_flag:       
            start = time() 
            im, persons = self.pe.processFrame(self.vs.fillFrameGrid(frames)) 
            print('FPS: ', 1/(time()-start))
            if persons is not None:            
                self.tr.distribute(persons)
                #print('FPS 2: ', 1/(time()-start))
                #rects, Xs = fast_personwise_normalize_all_body_25(self.tr.getPersons())                  
                rects, Xs = fast_personwise_normalize_all(self.tr.getPersons(),
                                                          dummy_slots[:self.tr.getPersonsCount()])                
                #print('FPS 2: ', 1/(time()-start))
                results = self.ac.predictAction(Xs.reshape(-1, 3, 50))     
                for i in range(len(rects)):
                    class_id = np.argmax(results[i])
                    im = self.vs.drawRectangle(im, *rects[i], rect_color=self.rect_colors[class_id],
                                               title=f'ID:{i} | {self.names_of_classes[class_id]}: {results[i][class_id]:.4f}')
            else:
                self.tr.resetPrevPersonsCount() #если в кадре нет людей, то обнуляем число отслеживаемых людей
                
            self.tr.timeStep()   
            print('FPS: ', 1/(time()-start))
            
            res = delay - 1000*(time()-start)
            self.vs.setDelay(ms=skip_frames*res if res > 1 else skip_frames)
    
            if not self.vs.showFrame(im):
                break
            
            for _ in range(skip_frames):
                self.vr.getFrames()
                
            return_flag, frames = self.vr.getFrames()
            print('TOTAL FPS: ', 1/(time()-start))
  
        self.vr.stopStreams()
        self.vs.cancel()
        
    def processVideoAndWrite(self, out_path, in_paths=[0], frames_in_row=2, frame_step=3, skip_frames=3, queue_size=32, frame_transform=None):
        self.vr.setSources(srcs=in_paths, 
                           queue_size=queue_size, 
                           transform=frame_transform)
        
        fps = self.vr.getFPS()        
        if not fps:
            fps = 20
        delay = 1000/fps #ms
        
        self.tr.resetState()
        dummy_slots = self.tr.getSlotsCopy() #сюда будут помещаться нормализованные скелеты людей
        
        return_flag, frames = self.vr.getFrames()  
        
        self.vs.setFrameGrid(frames_in_row, len(in_paths), *frames[0].shape)
        
        self.vw.setWriter(out_path, fps, (self.vs.getFrameGridShape()))  
        
        while return_flag:
            start = time()      
            im, persons = self.pe.processFrame(self.vs.fillFrameGrid(frames))                        
            if persons is not None:
                self.tr.distribute(persons)
                #rects, Xs = fast_personwise_normalize_all_body_25(self.tr.getPersons())    
                rects, Xs = fast_personwise_normalize_all(self.tr.getPersons(),
                                                          dummy_slots[:self.tr.getPersonsCount()])
                #results = self.ac.predictAction(Xs.reshape(-1, 3, 50))    
                for i in range(len(rects)):
                    #class_id = np.argmax(results[i])
                    im = self.vs.drawRectangle(im, *rects[i], rect_color=self.rect_colors[0],
                                               title=f'ID:{i}') #| {self.names_of_classes[class_id]}: {results[i][class_id]:.4f}')
            else:
                self.tr.resetPrevPersonsCount() #если в кадре нет людей, то обнуляем число отслеживаемых людей
                
            self.tr.timeStep()           
            res = delay - 1000*(time()-start)
            self.vs.setDelay(ms=3*res if res > 1 else 3)
          
            if not self.vs.showFrame(im):
                break
            
            for _ in range(skip_frames):
                self.vr.getFrames()
                self.vw.writeFrame(im) #замедляет выходное видео
            
            self.vw.writeFrame(im)            
            return_flag, frames = self.vr.getFrames()
            print('TOTAL FPS: ', 1/(time()-start))
  
        self.vr.stopStreams()
        self.vw.releaseFile()
        self.vs.cancel()
        
    
    def writeVideo(self, out_path, in_paths=[0], frames_in_row=2, frame_step=3, skip_frames=3, queue_size=32, frame_transform=None):
        self.vr.setSources(srcs=in_paths, 
                           queue_size=queue_size, 
                           transform=frame_transform)
        
        fps = self.vr.getFPS()      
        print(fps)
        if not fps:
            fps = 20
        
        self.vs.setDelay(ms=1000/fps)
        
        return_flag, frames = self.vr.getFrames()  
        
        self.vs.setFrameGrid(frames_in_row, len(in_paths), *frames[0].shape)
        
        self.vw.setWriter(out_path, fps, (self.vs.getFrameGridShape()))  
        
        while return_flag:   
            im = self.vs.fillFrameGrid(frames)                      
          
            if not self.vs.showFrame(im):
                break
            
            for _ in range(skip_frames):
                self.vr.getFrames()
            
            self.vw.writeFrame(im)            
            return_flag, frames = self.vr.getFrames()
  
        self.vr.stopStreams()
        self.vw.releaseFile()
        self.vs.cancel()
    

    def createTrainSet(self, out_path, class_id, in_path=0, skip_frames=0):
        self.vr.setSources([in_path]) 
        self.vs.setDelay(ms=1)
        points = pd.DataFrame(columns=[
            'Nose_x', 'Nose_y',
            
            'Chest_x', 'Chest_y',
            
            'R_Sho_x', 'R_Sho_y',
            'R_Elb_x', 'R_Elb_y',
            'R_Wr_x', 'R_Wr_y',
            
            'L_Sho_x', 'L_Sho_y',
            'L_Elb_x', 'L_Elb_y',
            'L_Wr_x', 'L_Wr_y',
            
            'Groin_x', 'Groin_y',
            
            'R_Hip_x', 'R_Hip_y',
            'R_Kn_x', 'R_Kn_y',
            'R-An_x', 'R-An_y',
            
            'L_Hip_x', 'L_Hip_y',
            'L_Kn_x', 'L_Kn_y', 
            'L_An_x', 'L_An_y',
            
            'R_Eye_x', 'R_Eye_y',            
            'L_Eye_x', 'L_Eye_y',
            
            'R_Ear_x', 'R_Ear_y',
            'L_Ear_x', 'L_Ear_y', 
            
            'L_Sol_x', 'L_Sol_y',
            'L_Toe_x', 'L_Toe_y',
            'L_Heel_x', 'L_Heel_y'
            ,
            'R_Sol_x', 'R_Sol_y',
            'R_Toe_x', 'R_Toe_y',
            'R_Heel_x', 'R_Heel_y',
            
            'Class'
        ])        
        if not os.path.exists(out_path):
            points.to_csv(out_path, header=True, index=False)
        
        return_flag, frames = self.vr.getFrames()
        while return_flag:
            cvOutputData, persons = self.pe.processFrame(frames[0]) 
            #cvOutputData = persons[0]
            if persons is not None: #предки были мудры (НЕ ТРОГАЙ, ЭТО ПРОГНЕВИТ БОГА-МАШИНУ)
                #persons = persons[:, :, :2] #отпиливыем вероятности
                points = pd.DataFrame(persons[0, :, :2].flatten()).transpose()
                points['Class'] = class_id
                points.to_csv(out_path, mode='a', header=False, index=False)
    
            if not self.vs.showFrame(cvOutputData):
                break
            
            for _ in range(skip_frames):
                self.vr.getFrames()
            
            return_flag, frames = self.vr.getFrames()
        self.vr.stopStreams()
        self.vs.cancel()