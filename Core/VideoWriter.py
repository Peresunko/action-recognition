import cv2

class VideoWriter():

    def __init__(self):
        self.writer = None
    
    def writeFrame(self, img):
        self.writer.write(img)
    
    
    def setWriter(self, out_path, fps, shape, fourcc='mp4v'):
        self.writer = cv2.VideoWriter(out_path,
                                      cv2.VideoWriter_fourcc(*fourcc),
                                      fps,
                                      shape)
                          
    def releaseFile(self):
        self.writer.release()