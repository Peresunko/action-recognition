import cv2
import numpy as np #HERE
from time import time #temp

class Visualizer():
    def __init__(self, title=None, delay=0):
        if not title:
            title='Human detection system'
        self.title = title
        self.delay = delay  
        self.frame_gird = None
        
    def setDelay(self, ms):
        self.delay = int(ms)  
    
    def setFrameGrid(self, cols, streams_count, cell_h, cell_w, cell_d):
        rows = streams_count // cols
        if streams_count % cols:
            rows += 1            
        
        self.__frame_grid = np.zeros((cell_h*rows, cell_w*cols, cell_d), dtype=np.uint8)
        
        self.__grid_cells = []
        for i in range(rows):
            for j in range(cols):
                self.__grid_cells.append(self.__frame_grid[i*cell_h:(i+1)*cell_h,
                                                       j*cell_w:(j+1)*cell_w, :])
                
        self.__no_signal_frame = np.zeros((cell_h, cell_w, cell_d), dtype=np.uint8) 
        
    def getFrameGridShape(self):
        if self.__frame_grid is not None:
            return self.__frame_grid.shape[1], self.__frame_grid.shape[0]
        return (0, 0)
  
    
    def fillFrameGrid(self, imgs):
        for i in range(len(imgs)):
            self.__grid_cells[i][:] = imgs[i] if imgs[i] is not None else self.__no_signal_frame
        return self.__frame_grid
    
    def showFrame(self, img):
        cv2.imshow(self.title, img)
        if cv2.waitKey(self.delay) == 27: # Клавиша Esc
            return False
        return True        
    
    def drawRectangle(self, image, left, top, right, bottom, title=None,
                      rect_color=(255, 255, 255), text_color=(0, 0, 0)):
        
        if right == 0 or bottom == 0:
            return image
        
        dx = (right - left) * 0.05
        dy = (bottom - top) * 0.05
        right += dx
        left -= dx
        top -= dy
        bottom += dy
        
        cv2.rectangle(img=image, pt1=(int(left), int(top)), pt2=(int(right), int(bottom)),
                      color=rect_color, thickness=2)
        
        if title:     
            cv2.rectangle(img=image, pt1=(int(left), int(top-30)), pt2=(int(left+len(title)*9), int(top)),
                          color=rect_color, thickness=-1)
            cv2.putText(img=image, text=title, org=(int(left+5), int(top-10)),
                        fontFace=cv2.FONT_HERSHEY_COMPLEX, fontScale=0.5,
                        color=text_color, thickness=1)
        return image
    
    def cancel(self):
        img = cv2.imread('Chii!.jpg')
        cv2.imshow(self.title, img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()