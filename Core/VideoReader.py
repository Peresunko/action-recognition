import cv2
import numpy as np
from imutils.video import FileVideoStream
#from imutils.video import WebcamVideoStream

class VideoReader():
    def __init__(self):
        self.fvs_list = None

        
    def setSources(self, srcs, channels=3, queue_size=128, transform=None):
        if self.fvs_list:
            self.stopStreams()
        self.fvs_list = [FileVideoStream(path=src, 
                                         transform=transform, 
                                         queue_size=queue_size).start() for src in srcs]
        
        self.frame_shape = (int(self.fvs_list[0].stream.get(cv2.CAP_PROP_FRAME_HEIGHT)),
                            int(self.fvs_list[0].stream.get(cv2.CAP_PROP_FRAME_WIDTH)),
                            channels)        
        
    def getFrames(self):
        frames = []
        for fvs in self.fvs_list:
            if fvs.more(): #проверка, что в очереди что-то есть, без нее система виснет намертво
                frames.append(fvs.read())  
            else:
                frames.append(None)
                
        if frames[0] is None:
            return False, frames
        return True, frames
    
    
    def getFPS(self): 
        if self.fvs_list: 
            return self.fvs_list[0].stream.get(cv2.CAP_PROP_FPS)
        else:
            return 0.0
        
    def getShape(self):
        if self.fvs_list:
            return self.frame_shape
        else:
            return (0, 0, 0)
        
    def stopStreams(self):
        for fvs in self.fvs_list:
            fvs.stop()