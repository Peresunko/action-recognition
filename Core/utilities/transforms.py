import cv2

class Scaler:
    def __init__(self, scale_factor):
        self.scale_f = scale_factor
    
    def __call__(self, img):
        if img is not None:
            return cv2.resize(img, (int(img.shape[1]*self.scale_f), 
                                    int(img.shape[0]*self.scale_f)))       
        return img