import cv2
import numpy as np
from .utilities.fast_tools import fast_var, fast_normalize_vec2

class OpenPose:
    def __init__(self, proto_file_path, caffe_weights_path, net_resolution=None):
        self.net = cv2.dnn.readNetFromCaffe(proto_file_path, caffe_weights_path)
        #CUDA
        self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
        
        self.keypoints_mapping = [
            'Nose', 'Chest', 
            'R-Sho', 'R-Elb', 'R-Wr', 
            'L-Sho', 'L-Elb', 'L-Wr', 
            'Groin', 
            'R-Hip', 'R-Kn', 'R-An',
            'L-Hip', 'L-Kn', 'L-An',
            'R-Eye', 'L-Eye',  
            'R-Ear', 'L-Ear', 
            'L-Sol', 'L-Toe', 'L-Heel',
            'R-Sol', 'R-Toe', 'R-Heel'
        ]
        
        self.pose_pairs = [
            (1, 0), (1, 2), (1, 5), (1, 8), (2, 3), (3, 4), (5, 6), (6, 7), 
            (8, 9), (9, 10), (10, 11), (11, 24), (11, 22), (22, 23), 
            (8, 12), (12, 13), (13, 14), (14, 21), (14, 19), (19, 20),
            (0, 15), (0, 16), (15, 17), (16, 18), (2, 17), (5, 18)
        ]
        
        self.paf_idx = [
            (56, 57), (40, 41), (48, 49), (26, 27), (42, 43), (44, 45), (50, 51), (52, 53), 
            (32, 33), (28, 29), (30, 31), (76, 77), (72, 73), (74, 75),
            (34, 35), (36, 37), (38, 39), (70, 71), (66, 67), (68, 69),
            (58, 59), (60, 61), (62, 63), (64, 65), (46, 47), (54, 55) 
        ]
        
        if net_resolution:
            self.net_h, self.net_w = net_resolution
        self.detected_keypoints = []
        self.keypoints_list = []
        self.valid_pairs = []
        self.humans = []        
 
    def setNetResolution(self, h, w):
        self.net_h, self.net_w = h, w

    def forward(self, image):     
        self.net.setInput(cv2.dnn.blobFromImages([image], (1.0 / 255.0), 
                                                (self.net_w, self.net_h), (127.5,)*3, swapRB=False, crop=False))
        self.net_output = self.net.forward()
    
    def getKeypoints(self, prob_map, threshold=0.1):
        map_smooth = cv2.GaussianBlur(prob_map, (3, 3), 0)

        map_mask = np.uint8(map_smooth > threshold)
        keypoints = []
    
        contours, _ = cv2.findContours(map_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
        for cnt in contours:            
            _, max_val, _, max_loc = cv2.minMaxLoc(map_smooth * cv2.fillConvexPoly(np.zeros(map_mask.shape), cnt, 1))    
            keypoints.append(max_loc + (prob_map[max_loc[1], max_loc[0]],))

        return keypoints    
        
    def detectKeypoints(self, threshold=0.1):  
        keypoints_list = []
        detected_keypoints = []
        keypoint_id = 0
        for i in range(len(self.keypoints_mapping)):            
            prob_map = cv2.resize(self.net_output[0, i, :, :], 
                                 (self.net_w, self.net_h),
                                 interpolation=cv2.INTER_LANCZOS4)
            
            keypoints = self.getKeypoints(prob_map, threshold)    

            keypoints_with_id = []
            for j in range(len(keypoints)):
                keypoints_with_id.append(keypoints[j] + (keypoint_id,))
                keypoints_list.append(keypoints[j])
                keypoint_id += 1

            detected_keypoints.append(keypoints_with_id)
            
        self.keypoints_list = keypoints_list
        self.detected_keypoints = detected_keypoints
        
    def buildValidPairs(self, paf_score_th=0.1, conf_th=0.7, n_interp_samples=20):
        valid_pairs = []
              
        for k in range(len(self.paf_idx)):     
            paf = [self.net_output[0, self.paf_idx[k][0], :, :], #X
                   self.net_output[0, self.paf_idx[k][1], :, :]] #Y
            
            paf[0] = cv2.resize(paf[0], (self.net_w, self.net_h), interpolation=cv2.INTER_LANCZOS4) #использовать разрешение сети
            paf[1] = cv2.resize(paf[1], (self.net_w, self.net_h), interpolation=cv2.INTER_LANCZOS4) #аналогично
            
            cands_A = self.detected_keypoints[self.pose_pairs[k][0]]
            cands_B = self.detected_keypoints[self.pose_pairs[k][1]]
   
            valid_pair = np.zeros((0, 4))
            for i in range(len(cands_A)):                
                max_j = -1
                max_score = -1.0               
                is_found = False
                for j in range(len(cands_B) ):                         
                    d_ij = np.subtract(cands_B[j][:2], cands_A[i][:2])                    
                    
                    if d_ij[0] == 0 and d_ij[1] == 0: #если точки совпали                        
                        continue                        
                   
                    d_ij = fast_normalize_vec2(d_ij)
                    
                    #p(u)
                    interpolate_coords = np.array((np.linspace(cands_A[i][0], cands_B[j][0], num=n_interp_samples),
                                                   np.linspace(cands_A[i][1], cands_B[j][1], num=n_interp_samples))).T
                    
                    #L(p(u))
                    paf_interpolate = []
                    for coord in interpolate_coords: 
                        #сначала [1] потом [0], т.к. paf - матрица, а coord - точка, поэтому для матрицы y-координата это номер строки
                        paf_interpolate.append((paf[0][int(round(coord[1])), int(round(coord[0]))], 
                                                paf[1][int(round(coord[1])), int(round(coord[0]))]))
                    
                    #E
                    paf_scores = np.dot(paf_interpolate, d_ij)
                    avg_paf_score = (sum(paf_scores) / len(paf_scores)) / (fast_var(paf_scores) + 1e-5)  #np.var(paf_scores)
                                             
                    #если доля интерполированных векторов, сонаправленных с PAF, выше порога -> допустимая пара
                    if (len(np.where(paf_scores > paf_score_th)[0]) / n_interp_samples) > conf_th:                        
                        if avg_paf_score > max_score:
                            max_j = j
                            max_score = avg_paf_score
                            is_found = True
             
                if is_found:
                    #cands_A[i][3], cands_B[max_j][3] - это порядковые номера точек (без учета их типа)
                    is_replaced = False
                    not_exist = False
                    for pair_id in range(len(valid_pairs)):
                        pair = valid_pairs[pair_id]
                        if cands_B[max_j][3] == pair[1] and k == pair[3]:
                            if max_score > pair[2]:
                                valid_pairs[pair_id] = (cands_A[i][3], cands_B[max_j][3], max_score, k)
                                is_replaced = True
                            else:
                                not_exist = True
                            break
                            
                    if not is_replaced and not not_exist:
                        valid_pairs.append((cands_A[i][3], cands_B[max_j][3], max_score, k))               
                           
        self.valid_pairs = valid_pairs
    
    def buildHumans(self):    
        humans = []
        empty_human = 26*[None]
        
        #создаем список человеков: каждый человек - это массив на 26 элементов под каждую конечноть, 
        #которые располагаются по порядку в соответствии с pose_pairs    

        for pair in self.valid_pairs:
            humans.append(26*[None])    
            humans[-1][pair[3]] = pair   
            
        length = len(humans)
        
        for i in range(len(humans)):  
            current_human = humans[i]
            
            if not current_human:
                continue

            for current_limb in current_human[:]:
                if not current_limb:
                    continue

                for j in range(length):
                    if j == i:
                        continue

                    another_human = humans[j]        

                    if not another_human:
                        continue

                    is_intersected = False

                    for another_limb in another_human:   
                        if not another_limb:
                            continue

                        for point_id in another_limb[:2]: #берем первые два элемента - это будут абсолютные индексы точек                        
                            if point_id in current_limb[:2]:                         
                                is_intersected = True                          
                                break

                        if is_intersected:                       
                            break

                    if is_intersected:           
                        for another_limb in another_human:   
                            if not another_limb:
                                continue
                            current_human[another_limb[3]] = another_limb   
                        humans[j] = None                                          
    
        perfect_humans = []
        for human in humans:
            if not human or not human[3]: #сделать что-то с этой личинкой (3)
                continue
            perfect_humans.append(25*[(0, 0)])
            for j, limb in enumerate(human):
                if not limb:
                    continue
                else:
                    perfect_humans[-1][self.pose_pairs[j][0]] = self.keypoints_list[limb[0]][:2]
                    perfect_humans[-1][self.pose_pairs[j][1]] = self.keypoints_list[limb[1]][:2]
                        
        self.humans = np.array(perfect_humans)
        
        
    def processImage(self, image, keypoints_th=0.25,
                     paf_score_th=0.1, conf_th=0.7, 
                     n_interp_samples=20):
        self.forward(image)
        self.detectKeypoints(keypoints_th)
        self.buildValidPairs(paf_score_th, conf_th, n_interp_samples)
        self.buildHumans()
        return self.humans