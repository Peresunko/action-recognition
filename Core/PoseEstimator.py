from .gpu_openpose import pyopenpose as op

class PoseEstimator():
    def __init__(self, params=None):        
        if not params:
            params = dict()
            params['model_folder'] = 'Core/HumanDetectModel'
            params['model_pose'] = 'BODY_25'
            params['net_resolution'] = '-1x160'
        
        self.net = op.WrapperPython()
        self.net.configure(params)
        self.net.start()
        self.datum = op.Datum()
        
    def processFrame(self, frame):
        self.datum.cvInputData = frame
        self.net.emplaceAndPop(op.VectorDatum([self.datum]))
        return self.datum.cvOutputData, self.datum.poseKeypoints[:, :, :2] #отпиливыем вероятности    